<?php
namespace StructuredContentPlugin\Handler;

use StructuredContentPlugin\Factories\Abstracts\Enqueue;

class enqueueStyles extends Enqueue {

	/**
	 * Enqueue styles in front, backend or login.
	 *
	 * enqueue() use following actionHook to include styles
	 * @wpHook wp_enqueue_scripts — frontend
	 * @wpHook admin_enqueue_scripts — adminBackend
	 * @wpHook login_enqueue_scripts — loginPage
	 *
	 * @param array $styles //  [ 'myCustomCssID' => [
	 *                                  'src'     => $baseDir . 'assets/css/my-custom.css',
	 *                                  'deps'    => NULL,
	 *                                  'version' => NULL,
	 *                                  'media'   => NULL,
	 *                                  'loadAt'  => 'admin | login | default: frontend'
	 *                                 ]
	 *                            ]
	 * @param string $baseUrl
	 *
	 * @return void
	 */
	public function __construct( $styles,  $baseUrl) {

		if(!empty($styles) && !empty($baseUrl)) {
			$assets = $this->gropeAssetsByType($styles, $baseUrl, 'css');

			if ( ! empty( $assets[ 'login' ] ) ) {
				add_action( 'login_enqueue_scripts', function () use ( $assets ) {
					parent::enqueue( $assets[ 'login' ], 'style' );
				} );
			}

			if ( ! empty( $assets[ 'admin' ] ) ) {
				add_action( 'admin_enqueue_scripts', function () use ( $assets ) {
					parent::enqueue( $assets[ 'admin' ], 'style' );
				} );
			}

			if ( ! empty( $assets[ 'frontend' ] ) ) {
				add_action( 'wp_enqueue_scripts', function () use ( $assets ) {
					parent::enqueue( $assets[ 'frontend' ], 'style' );
				} );
			}

		}

	}


}
