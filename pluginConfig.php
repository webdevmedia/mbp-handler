<?php # -*- coding: utf-8 -*-

namespace StructuredContentPlugin\Handler;

use StructuredContentPlugin\Factories;

/**
 * Class Configs
 *
 * @package StructuredContentPlugin\Factories
 */
class pluginConfig extends Factories\Abstracts\Config {

	/**
	 * @param string $moduleName
	 * @param array  $moduleConfigs
	 *
	 * @return enqueue
	 */
	public function enqueue(string $moduleName, array $moduleConfigs, string $moduleBasePath): enqueue{

		try {
			if(!empty($moduleName) || !empty($moduleConfigs)) {

				$moduleStyles = !empty($moduleConfigs['styles']) ? $moduleConfigs['styles'] : [];
				$moduleScripts = !empty($moduleConfigs['scripts']) ? $moduleConfigs['scripts'] : [];

				$enqueue =  new enqueue($moduleName, parent::getSetting());
				$enqueue->addStyles($moduleStyles);
				$enqueue->addScripts($moduleScripts);

				new enqueueStyles($enqueue->getSetting('enqueue', 'styles'), $moduleBasePath);
				new enqueueScripts($enqueue->getSetting('enqueue', 'scripts'), $moduleBasePath);

				return $enqueue;
			}else{
				if(!empty($moduleName)){
					throw new \Exception('$moduleName is empty!');
				}elseif(!empty($moduleConfigs)){
					throw new \Exception('$moduleConfigs is empty');
				}
			}

		} catch (\Exception $e) {
			echo '<pre>';print_r( [ 'Location' => [ 'PATH' => dirname( __FILE__ ), 'FILE' => basename( __FILE__ ), 'FUNCTION' => __FUNCTION__ . ':' . __LINE__ ], 'Exception' => [
				'msg' => $e->getMessage()
			]] );
			die();
		}

	}
}
