<?php # -*- coding: utf-8 -*-

namespace StructuredContentPlugin\Handler;

use StructuredContentPlugin\Factories;

/**
 * Class Configs
 *
 * @package StructuredContentPlugin\Factories
 */
class enqueue extends Factories\Abstracts\Config {

	private static $index = '';

	private $settings = [];

	/**
	 * @var Factories\Abstracts\Config
	 */
	private $pluginConfig;

	public function __construct(string $moduleName, array $settings) {
		$this->settings = $settings;

		$this->setIndex($moduleName);
	}

	public function addStyles(array $styles){

		if(empty(self::getIndex())){
			return false;
		}

		parent::setEntities($this->settings);

		$this->add('enqueue', ['styles' => $this->conclude( self::getIndex(), $styles) ]);

        return parent::getSetting('enqueue', 'styles');
	}

	public function addScripts(array $scripts){

		if(empty(self::getIndex())){
			return false;
		}

		$config = $this->add('enqueue', ['scripts' => $this->conclude( self::getIndex(), $scripts) ]);

		return parent::getSetting('enqueue');
	}

	private function conclude($index, $array): array{
		$modulated = [];
		$items = !empty($array) ? $array : [];

		foreach($items as $i => $v){
			$modulated[$index . '-' .  $i] = $v;
		}

		return $modulated;
	}

	/**
	 * @return string
	 */
	private function getIndex(): ?string {
		return !empty(self::$index) ? self::$index : null;
	}

	/**
	 * @param string $index
	 *
	 * @return enqueue
	 */
	private function setIndex( string $index ): enqueue {
		self::$index = !empty($index) ? $index : null;
		return $this;
	}

}
