<?php
namespace StructuredContentPlugin\Handler;

use StructuredContentPlugin\Factories\Abstracts\Enqueue;

class enqueueScripts extends Enqueue {


    /**
     * Enqueue scripts in front, backend or login.
     *
     * enqueue() use following actionHook to include styles
     * @wpHook wp_enqueue_scripts — frontend
     * @wpHook admin_enqueue_scripts — adminBackend
     * @wpHook login_enqueue_scripts — loginPage
     *
     * @param array scripts //  [ 'myCustomScriptID' => [
     *                                  'src'     => $baseDir . 'assets/css/my-custom.js',
     *                                  'deps'    => NULL,
     *                                  'version' => NULL,
     *                                  'media'   => NULL,
     *                                  'loadAt'  => 'admin | login | default: frontend'
     *                                 ]
     *                            ]
     * @param string $baseUrl
     *
     * @return void
     */
    public function __construct( $scripts,  $baseUrl) {

        if(!empty($scripts) && !empty($baseUrl)) {
            $assets = $this->gropeAssetsByType($scripts, $baseUrl, 'js');

	        if(!empty($assets['login'])) {
		        add_action( 'login_enqueue_scripts', function () use ( $assets ) {
			        parent::enqueue( $assets[ 'login' ], 'script' );
		        } );
	        }

	        if ( ! empty( $assets[ 'admin' ] ) ) {
		        add_action( 'admin_enqueue_scripts', function () use ( $assets ) {
			        parent::enqueue( $assets[ 'admin' ], 'script' );
		        } );

                add_action('admin_head', function() use ($baseUrl){
                        echo '<script type="text/javascript">
		                    var ajaxurl = "' . admin_url('admin-ajax.php') . '";
		                    var moduleName = "' . basename($baseUrl) . '";
		                 </script>';
                });
	        }

			if(! empty( $assets[ 'frontend' ] ) ) {
				add_action( 'wp_enqueue_scripts', function () use ( $assets ) {
					parent::enqueue( $assets[ 'frontend' ], 'script' );
				} );

                add_action('wp_head', function() use ($baseUrl){
                    echo '<script type="text/javascript">
		                    var ajaxurl = "' . admin_url('admin-ajax.php') . '";
		                    var moduleName = "' . basename($baseUrl) . '";
		                 </script>';
                });
			}




        }

    }

}
