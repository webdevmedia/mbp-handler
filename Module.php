<?php
namespace StructuredContentPlugin\Handler;

use StructuredContentPlugin\Factories;

class Module extends Factories\Abstracts\Entity {

	/**
	 * @var Handler\Common
	 */
	private $common;

	/**
	 * @var Handler\pluginConfig
	 */
	public $pluginConfig;

	/**
	 * @var string
	 */
	public $moduleName = '';

	public $moduleBasePath = '';

	public function __construct(pluginConfig $pluginConfig) {
		$this->common = new Common();

		$this->pluginConfig = $pluginConfig;

		$this->moduleName = $this->common->callee(3, 1, true)->getModuleName();
		$this->moduleBasePath = $this->common->callee(3, 1, true)->getModulePath();

		$this->setAssets();
	}

	public function setAssets(){
		$entitiesPath = $this->moduleBasePath . '/Configs';
		$this->loadEntities($entitiesPath, 'json', true);

		$moduleConfig = $this->get();

		if(!empty($moduleConfig['enqueue'])) {
			$this->pluginConfig->enqueue( $this->moduleName, $moduleConfig[ 'enqueue' ], $this->moduleBasePath );
		}
	}

	public static function getModule($moduleName){
		$modules = Factories\Module::getModules();

		if(!strpos( $moduleName, 'Module')){
			$moduleName = 'Module\\' . $moduleName;
		}

		try {
			if(!empty($modules[$moduleName])) {
				return $modules[$moduleName];
			}else{
				throw new \Exception("Module: " . $moduleName . " not found!");
			}

		} catch (\Exception $e) {
			echo '<pre>';print_r( [ 'Location' => [ 'PATH' => dirname( __FILE__ ), 'FILE' => basename( __FILE__ ), 'FUNCTION' => __FUNCTION__ . ':' . __LINE__ ], 'Exception' => [
				'msg' => $e->getMessage(),
				'$moduleName' => $moduleName
			]] );
			die();
		}
	}

	public static function getModules(){
		return Factories\Module::getModules();
	}

}
