<?php
namespace StructuredContentPlugin\Handler;

#test

class Common  {

	private  $callee = [];

	/**
	 * Returns the moduleName and Path and optional a full backtrace
	 *
	 * @param int|NULL $deep
	 * @param int|NULL $index
	 * @param bool     $fullStack
	 *
	 * @return array
	 */
	public function callee(int $deep = null, int $index = null, bool $fullStack = false):Common {
		$index = (!empty($index)) ? $index : 0;

		$dbt = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, $deep);
		$traceDirname = (!empty($dbt[$index]['file']) )? $dbt[$index]['file'] : null;

		if(!empty($traceDirname)){
			preg_match('/((?<moduleBasePath>^\/.*?\/Module\/(?<moduleName>.*?)\/))/i', $traceDirname, $match);
			$moduleName = !empty($match['moduleName']) ? $match['moduleName'] : '';
			$moduleBasePath = !empty($match['moduleBasePath']) ? $match['moduleBasePath'] : '';
		}

		if(!empty($moduleName)) {
			$this->callee = [
				'moduleBasePath' => $moduleBasePath,
				'moduleName'     => $moduleName
			];
		}

		if($fullStack === true){
			$this->callee['fullStack'] = $dbt;
		}

		return $this;
	}

	public function getModuleName(): string{
		return !empty($this->callee['moduleName']) ? $this->callee['moduleName'] : 'undefined';
	}

	public function getModulePath(): string{
		return !empty($this->callee['moduleBasePath']) ? $this->callee['moduleBasePath'] : 'undefined';
	}

}
